import com.jhlabs.image.BoxBlurFilter;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.servlet.MultipartConfigElement;
import javax.servlet.http.Part;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferInt;
import java.awt.image.WritableRaster;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Iterator;


import static jdk.nashorn.internal.objects.Global.print;
import static spark.Spark.*;
import static spark.route.HttpMethod.post;

/**
 * @Author Tek Raj Chhetri
 * 	This Java program uses the Spark web application framework to run a webserver.
 *  See http://sparkjava.com/ for details on Spark.
 */
public class Main {
  public static void main(String... args) throws Exception {

	// Tell Spark to use the Environment variable "PORT" set by Heroku. If no PORT variable is set, default to port 5000.
	int port = System.getenv("PORT")== null ? 5000 : Integer.valueOf(System.getenv("PORT"));
	port(port);


	  get("/", (req, res) -> {
	  	String info = "<h1 style=\"color: #5e9ca0; text-align: center;\">Image Processing API- Home Assignment</h1>\n" +
				"<h1 style=\"color: #5e9ca0; text-align: center;\">Institute of Computer Science</h1>\n" +
				"<h1 style=\"color: #5e9ca0; text-align: center;\">University of Tartu</h1>\n" +
				"<h1 style=\"color: #5e9ca0; text-align: center;\">Tek Raj Chhetri</h1>\n" +
				"<p>&nbsp;</p>\n" +
				"<p>&nbsp;</p>\n" +
				"<h2 style=\"color: #2e6c80;\">Usage:</h2>\n" +
				"<p>Make an Http&nbsp;Post Request to&nbsp;&nbsp;<strong>baseURL/simple&nbsp;</strong>and this returns the converted (Blurred) image in png format.</p>\n" +
				"<p>Eg:</p>\n" +
				"<p>Makinng&nbsp;http request</p>\n" +
				"<p>baseUrl = <a href=\"https://cloudimageprocessingapp.herokuapp.com/\">https://cloudimageprocessingapp.herokuapp.com/</a></p>\n" +
				"<pre>Request request = new Request.Builder()<br />        .url(baseUrl+\"/simple\")<br />        .post(image)<br />        .build();</pre>\n" +
				"<p>&nbsp;</p>";
	  	return info;
	  });

// matches "GET /hello/foo" and "GET /hello/bar"
// request.params(":name") is 'foo' or 'bar'
	  get("/hello/:name", (request, response) -> {
	  	System.out.println("hello clicked");
		  return "Hello: " + request.params(":name");

	  });

	  post("/test", (req, res) -> {
	  	System.out.println("test click");
		  return "this is post>>"+req.body();
	  });

	


	// this route gets the image from the user
	  // upload to the folder named image in server
	  // converts image i.e. make blur
	  // and sends back to the user blured image as response
	post("/simple","multipart/form-data", (request, response) -> {

		System.out.println("ACTIVATED");
		String location = "image";          // the directory location where files will be stored
		long maxFileSize = 1000000000;       // the maximum size allowed for uploaded files
		long maxRequestSize = 1000000000;    // the maximum size allowed for multipart/form-data requests
		int fileSizeThreshold = 1000000024;       // the size threshold after which files will be written to disk

		MultipartConfigElement multipartConfigElement = new MultipartConfigElement(
				location, maxFileSize, maxRequestSize, fileSizeThreshold);
		request.raw().setAttribute("org.eclipse.jetty.multipartConfig",
				multipartConfigElement);

		Collection<Part> parts = request.raw().getParts();
		for (Part part : parts) {

			System.out.println("Name: " + part.getName());
			System.out.println("Size: " + part.getSize());
			System.out.println("Filename: " + part.getSubmittedFileName());
		}

		String fName = request.raw().getPart("file").getSubmittedFileName();
		System.out.println("Title: " + request.raw().getParameter("title"));
		System.out.println("File: " + fName);

		Part uploadedFile = request.raw().getPart("file");
		Path out = Paths.get("image/" + fName);

		try (final InputStream in = uploadedFile.getInputStream()) {
			//delete if exist and same file come next time
			// this is because file might be same but not the image
			Files.deleteIfExists(out);

			Files.copy(in, out);
			uploadedFile.delete();
		}catch (Exception ex){
			ex.printStackTrace();
		}
		// cleanup
		multipartConfigElement = null;
		parts = null;
		uploadedFile = null;

		File file = new File(String.valueOf(out));

		BufferedImage img = ImageIO.read(file);


		int width = img.getWidth();
		int height = img.getHeight();
		int[][] pixels = new int[width][height];
		BufferedImage newBI = new BufferedImage(width,height,BufferedImage.TYPE_INT_ARGB);
		for (int i=0; i < width;i++){
			for (int j=0; j<height;j++){
				pixels[i][j] = img.getRGB(i,j);             }
		}

		BoxBlurFilter boxBlurFilter = new BoxBlurFilter();
		boxBlurFilter.setRadius(10);
		boxBlurFilter.setIterations(10);
		boxBlurFilter.filter(img,newBI);

		WritableRaster writableRaster  =  newBI.getRaster();


		String newName = "image/result_blured.jpg";

		//Files.deleteIfExists(Paths.get(newName));
		ImageIO.write(newBI,"png", new File(newName));

		BufferedImage converted = ImageIO.read(new File(newName));
		byte [] bluredImage = ((DataBufferByte) converted.getRaster().getDataBuffer()).getData();
		//int[] bluredImageInt = ((DataBufferInt) newBI.getRaster().getDataBuffer()).getData();


		byte[] rawImage = null;
		try(ByteArrayOutputStream baos = new ByteArrayOutputStream()) {

			ImageIO.write( converted, "png", baos );

			baos.flush();
			rawImage = baos.toByteArray();
		}

		response.type("image/png");
		return rawImage;



	});

  }
}