**Cloud Image Processing API**


## Usage

Make an Http Post Request to  baseURL/simple and this returns the converted (Blurred) image in png format.

**Eg:**
Makinng http request

baseUrl = https://cloudimageprocessingapp.herokuapp.com/

Request request = new Request.Builder()
        .url(baseUrl+"/simple")
        .post(image)
        .build();
---
